package rip.devotedpvp.skyfortress.map;

import rip.devotedpvp.api.framework.util.mc.config.LocationUtil;
import rip.devotedpvp.api.framework.util.mc.world.LocationObject;
import rip.devotedpvp.api.game.maps.GameMap;
import rip.devotedpvp.api.game.maps.MapData;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.*;


public class SkyFortressMap extends GameMap {
    public SkyFortressMap(String name, MapData data, File yml, File zip) {
        super(name, data, yml, zip);
    }

    @SuppressWarnings("unchecked")
    public Map loadSetting(ConfigurationSection configurationSection) {
        Map map = new HashMap<>();

        List<SkyIsland> islands = new ArrayList<>();
        Set<Cord> cords = new HashSet<>();
        for (String island : configurationSection.getConfigurationSection("islands").getKeys(false)) {
            String section = "islands." + island;
            LocationObject spawn = LocationUtil.fromConfig(configurationSection.getConfigurationSection(section + ".spawn"));
            Set<LocationObject> chests = new HashSet<>();
            for (String chest : configurationSection.getConfigurationSection(section + ".chests").getKeys(false)) {
                LocationObject object = LocationUtil.fromConfig(configurationSection.getConfigurationSection(section + ".chests." + chest));
                chests.add(object);
                cords.add(Cord.fromLocation(object));
            }
            islands.add(new SkyIsland(chests, spawn));
        }
        map.put("islands", islands);

        map.put("cords", cords);

        Set<LocationObject> guards = new HashSet<>();
        for (String guard : configurationSection.getConfigurationSection("guards.wither").getKeys(false)) {
            guards.add(LocationUtil.fromConfig(configurationSection.getConfigurationSection("guards.wither." + guard)));
        }
        map.put("witherguards", guards);

        LocationObject crownlocation = LocationUtil.fromConfig(configurationSection.getConfigurationSection("crown"));
        map.put("crown", crownlocation);
        return map;
    }

    @SuppressWarnings("unchecked")
    public List<SkyIsland> getIslands() {
        return (List<SkyIsland>) getSettings().get("islands");
    }

    @SuppressWarnings("unchecked")
    public Set<LocationObject> getGuardLocations() {
        return (Set<LocationObject>) getSettings().get("witherguards");
    }

    @SuppressWarnings("unchecked")
    public Set<Cord> getCordSet() {
        return (Set<Cord>) getSettings().get("cords");
    }

    @SuppressWarnings("unchecked")
    public LocationObject getCrownLocation() {
        return (LocationObject) getSettings().get("crown");
    }
}
